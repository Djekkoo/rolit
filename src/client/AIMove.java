package client;

import rolit.CallbackException;
import rolit.Move;
import rolit.Callback;

public class AIMove implements Runnable {

	private Move move;
	private Callback wakeup;
	private Callback set;

	public AIMove(Move move, Callback wakeup, Callback set) {
		super();
		this.move = move;
		this.wakeup = wakeup;
		this.set = set;
	}
	
	@Override
	public void run() {

		try {
			Thread.sleep(750);
			this.wakeup.invoke();
			this.set.invoke(move.getI());
		} catch (CallbackException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
