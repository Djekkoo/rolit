package client;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import rolit.Callback;
import rolit.CallbackException;

import com.esotericsoftware.tablelayout.swing.Table;

public class LoginGUI extends JFrame {

	private static final long serialVersionUID = -4289768183738097908L;
	private Callback callback;
	private JTextField host;
	private JTextField port;
	private JTextField username;
	private JComboBox<?> gametype;
	protected JPasswordField password;
	private JCheckBox ai;
	
	public LoginGUI(Callback callback) {
		super();
		this.callback = callback;
		
		initGUI();
		
	}
	
	private void initGUI() {
		
//		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		Table table = new Table();
		this.add(table);
		
		ActionListener start = new LoginController(this);

		this.host = new JTextField("127.0.0.1");
		this.port = new JTextField("2014");
		this.username = new JTextField();
		this.password = new JPasswordField();
		this.ai	= new JCheckBox();
		this.gametype = new JComboBox<String>(new String[] {
				"2 Players",
				"3 Players",
				"4 Players",
				"Default game"
		});

		this.username.addKeyListener(new kListener(new Callback(start, "actionPerformed")));
		this.password.addKeyListener(new kListener(new Callback(start, "actionPerformed")));
		
		table.addCell(new JLabel("Host: ")).width(80f).left();
		table.addCell(this.host).width(150f);
		table.addCell(new JLabel(" : ")).width(10f);
		table.addCell(this.port).width(40f);
		table.row();
		
		table.addCell(new JLabel("Username: ")).left();
		table.addCell(this.username).colspan(3).fillX();
		table.row();

		table.addCell(new JLabel("Password: ")).left();
		table.addCell(this.password).colspan(3).fillX();
		table.row();
		
		table.addCell(new JLabel("Gametype: ")).left();
		table.addCell(this.gametype).colspan(3).fillX();
		table.row();
		
		table.addCell(new JLabel("Use Artificial intelligence: ")).colspan(1).left();
		table.addCell(this.ai).colspan(3).fillX().left();
		table.row();

		table.addCell(new JLabel(" ")).colspan(4);
		table.row();
		
		JButton button = new JButton("Login");
		button.addActionListener(start);
		table.addCell(button).colspan(4).right().fillX();
		table.row();
		
		this.setMinimumSize(new Dimension(380, 210));
		this.setSize(this.getMinimumSize());
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
	}

	private class kListener implements KeyListener {
		
		private Callback cb;
		
		public kListener(Callback cb) {
			super();
			this.cb = cb;
		}
		
		public void keyPressed(KeyEvent arg0)  {}

		public void keyReleased(KeyEvent arg0) {}

		public void keyTyped(KeyEvent arg0) {
			if (arg0.getKeyChar() == '\n') {
			
				try {
					cb.invoke();
				} catch (CallbackException e) {}
				
			}
		}
		
	}
	
	public class LoginController implements ActionListener {

		private LoginGUI loginGUI;

		public LoginController(LoginGUI loginGUI) {
			this.loginGUI = loginGUI;
			
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			this.actionPerformed();
			
		}
		
		public void actionPerformed() {
			// is port an integer?
			if (!loginGUI.port.getText().matches("^[1-9]([0-9]+)?$")) {
				JOptionPane.showMessageDialog(this.loginGUI, "The given port is invalid", "Error", JOptionPane.OK_OPTION);
				return;
			}
			// host contains portnumber
			if (loginGUI.host.getText().contains(":")) {
				JOptionPane.showMessageDialog(this.loginGUI, "The host should not contain an port-specification (:)", "Error", JOptionPane.OK_OPTION);
				return;
			}
			// empty username or password
			if (loginGUI.username.getText().equals("") || loginGUI.password.getPassword().length == 0) {
				JOptionPane.showMessageDialog(this.loginGUI, "The username and password should be specified", "Error", JOptionPane.OK_OPTION);
				return;
			}

			// error in gamestate
			if (loginGUI.gametype.getSelectedObjects().length != 1) {
				JOptionPane.showMessageDialog(this.loginGUI, "The gametype should be specified", "Error", JOptionPane.OK_OPTION);
				return;
			}
			
			try {
				loginGUI.callback.invoke(
					loginGUI.username.getText(),
					new String(loginGUI.password.getPassword()),
					loginGUI.host.getText(),
					Integer.valueOf(loginGUI.port.getText()),
					loginGUI.ai.isSelected(),
					loginGUI.gametype.getSelectedObjects()[0]
				);
			} catch (NumberFormatException | CallbackException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void showPopup(String title, String message) {
		JOptionPane.showMessageDialog(this, message, title, JOptionPane.OK_OPTION);
	}
	
	public void close() {
		
		this.setEnabled(false);
		this.setVisible(false);
		
	}
	public void open() {
		
		this.setEnabled(true);
		this.setSize(390, 190);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
	}

	
}
