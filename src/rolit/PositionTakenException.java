package rolit;

public class PositionTakenException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3379371672013726387L;

	public PositionTakenException(int x, int y) {
		super("Position: ["+x+","+y+"] is already taken!");
	}
	
}