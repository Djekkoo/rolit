/**
 * 
 */
package rolit;

import java.util.HashSet;
import java.util.Observable;
import java.util.Set;

/**
 * @author Sander, Jacco
 *
 */
public class Board extends Observable {
	
	public static final int BOARD_SIZE = 12;
	private Player[][] board = new Player[BOARD_SIZE][BOARD_SIZE];
	private Player[] players;
	
	public Board(){}
	public Board(String board, Player[] players){
		make(board, players);
	}
	
	public void make(String board, Player[] players){

		this.players = players;
		
		// if the length is incorrect, die();
		if ((int) Math.pow(BOARD_SIZE,2) * 2 - 1 != board.trim().length()) return;
		
		int z = 0;
		boolean found = false;
		
		int index = 0;
		
		for (int j = 0; j < BOARD_SIZE; j++) {
			for (int i = 0; i < BOARD_SIZE; i++) {
				index = (j * BOARD_SIZE + i) * 2; // Should be the position in the string
				z = Integer.parseInt(board.substring(index, index + 1)) - 1;
				if (z == -1) {
					put(i,j,null);
				} else {
					found = false;
					for(Player p : players){
						if(p.getPosition() == z){
							put(i, j, p);
							found = true;
						} 
					}
					if(!found) {
						put(i, j, new Player("",z,players[0].getBasePosition()));
					}
				}
			}
		}
	}
	
	public Board(Player[][] board) {
		this.board = board;
	}
	
	public void init(Player[] Players) {
		
		this.players = Players;
		
		if (this.getEmpty().length < BOARD_SIZE * BOARD_SIZE)
			return;
		
		switch (Players.length) {
			case 2: {
				this.put((int)(BOARD_SIZE / 2)-1, (int)(BOARD_SIZE / 2)-1, Players[0]);
				this.put((int)(BOARD_SIZE / 2), (int)(BOARD_SIZE / 2)-1, Players[1]);
				this.put((int)(BOARD_SIZE / 2), (int)(BOARD_SIZE / 2), new Player("", 2, Players[0].getBasePosition()));
				this.put((int)(BOARD_SIZE / 2)-1, (int)(BOARD_SIZE / 2), new Player("", 3, Players[0].getBasePosition()));
				break;
			}
			case 3: {
				this.put((int)(BOARD_SIZE / 2)-1, (int)(BOARD_SIZE / 2)-1, Players[0]);
				this.put((int)(BOARD_SIZE / 2), (int)(BOARD_SIZE / 2)-1, Players[1]);
				this.put((int)(BOARD_SIZE / 2), (int)(BOARD_SIZE / 2), Players[2]);
				this.put((int)(BOARD_SIZE / 2)-1, (int)(BOARD_SIZE / 2), new Player("", 3, Players[0].getBasePosition()));
				break;
			}
			case 4: {
				this.put((int)(BOARD_SIZE / 2)-1, (int)(BOARD_SIZE / 2)-1, Players[0]);
				this.put((int)(BOARD_SIZE / 2), (int)(BOARD_SIZE / 2)-1, Players[1]);
				this.put((int)(BOARD_SIZE / 2), (int)(BOARD_SIZE / 2), Players[2]);
				this.put((int)(BOARD_SIZE / 2)-1, (int)(BOARD_SIZE / 2), Players[3]);
				break;
				
			}
		}
		
	}
	
	public Board deepCopy() {
		
		return new Board(this.toString(), this.players);
	}

	public Player get(int x, int y) {
		return this.board[y][x];
	}
	public Player get(int i) {
		return get(i % BOARD_SIZE, (int)Math.floor(i / BOARD_SIZE));
	}
	
	public void set(Move move) throws PositionTakenException, NotTouchingException, CapturingException {
		set(move.getX(), move.getY(), move.getPlayer());
	}
	
	public void set(int i, Player player) throws PositionTakenException, NotTouchingException, CapturingException {
		set(i % BOARD_SIZE, (int)Math.floor(i / BOARD_SIZE), player);
	}
	
	public synchronized void set(int x, int y, Player player) throws PositionTakenException, NotTouchingException, CapturingException {
		
		// is valid
		try {
			this.isAllowed(x, y, player, true);
		} catch(Exception e) {
			throw e;
		}
		
		// appareantly the set is valid.
		// No other balls?
		boolean last = true;
		for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
			if (player.equals(board[(int)Math.floor(i / BOARD_SIZE)][i % BOARD_SIZE])) {
				last = false;
				break;
			}
		}
		// No balls to capture&block with, just place it.
		if (last) {
			this.put(x, y, player);
			return;
		}
		
		
		// horizontal <x
		int i = Math.max(x-1, 0);
		while (i > 0 && !(board[y][i] == null) && !board[y][i].equals(player)) {
			i--;
		}
		if (board[y][i] != null && board[y][i].equals(player)) {
			for (int c = x; c >= i; c--)
				this.put(c, y, player);
		} 
		
		// horizontal >x
		i = Math.min(x+1, BOARD_SIZE - 1);
		while (i < BOARD_SIZE-1 && !(board[y][i] == null) && !board[y][i].equals(player)) {
			i++;
		}
		if (board[y][i] != null && board[y][i].equals(player)) {
			for (int c = x; c <= i; c++)
				this.put(c, y, player);
		}
		
		// vertical <y
		i = Math.max(y-1, 0);
		while (i > 0 && !(board[i][x] == null) && !board[i][x].equals(player)) {
			i--;
		}
		if (board[i][x] != null && board[i][x].equals(player)) {
			for (int c = y; c >= i; c--)
				this.put(x, c, player);
		}
		
		// vertical >y
		i = Math.min(y+1, BOARD_SIZE - 1);
		while (i < BOARD_SIZE-1 && !(board[i][x] == null) && !board[i][x].equals(player)) {
			i++;
		}
		if (board[i][x] != null && board[i][x].equals(player)) {
			for (int c = y; c < i; c++)
				this.put(x, c, player);
		}

		// diagonal <y <x
		i = Math.max(x-1, 0);
		int j = Math.max(y-1, 0);
		while (i > 0 && j > 0 && !(board[j][i] == null) && !board[j][i].equals(player)) {
			i--;
			j--;
		}
		if (board[j][i] != null && board[j][i].equals(player)) {
			for (int c = 0; c < Math.abs(x-i); c++)
				this.put(x-c, y-c, player);
		}

		// diagonal >y >x
		i = Math.min(x+1, BOARD_SIZE - 1);
		j = Math.min(y+1, BOARD_SIZE - 1);
		while (i < BOARD_SIZE-1 && j < BOARD_SIZE-1 && !(board[j][i] == null) && !board[j][i].equals(player)) {
			i++;
			j++;
		}
		if (board[j][i] != null && board[j][i].equals(player)) {
			for (int c = 0; c < Math.abs(x-i); c++)
				this.put(x+c, y+c, player);
		}

		// diagonal >y <x
		i = Math.max(x-1, 0);
		j = Math.min(y+1, BOARD_SIZE - 1);
		while (i > 0 && j < BOARD_SIZE-1 && !(board[j][i] == null) && !board[j][i].equals(player)) {
			i--;
			j++;
		}
		if (board[j][i] != null && board[j][i].equals(player)) {
			for (int c = 0; c < Math.abs(x-i); c++)
				this.put(x-c, y+c, player);
		}

		// diagonal <y >x
		i = Math.min(x+1, BOARD_SIZE - 1);
		j = Math.max(y-1, 0);
		while (i < BOARD_SIZE-1 && j > 0 && !(board[j][i] == null) && !board[j][i].equals(player)) {
			i++;
			j--;
		}
		if (board[j][i] != null && board[j][i].equals(player)) {
			for (int c = 0; c < Math.abs(x-i); c++)
				this.put(x+c, y-c, player);
		}
		
	}
	
	public boolean isAllowed(int x, int y, Player player) {
		
		// check if set is allowed (is it taken?)
		if (board[y][x] != null) return false;
		
		if (!isTouching(x, y)) return false;
		
		// is it capturing?
		boolean capturing = this.isCapturing(x, y, player);
		if (!capturing) {
			for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
				// Possible move?
				if (board[(int)Math.floor(i / BOARD_SIZE)][i % BOARD_SIZE] == null && isTouching(i % BOARD_SIZE, (int)Math.floor(i / BOARD_SIZE))) {
					// Capturing?
					if (this.isCapturing(i % BOARD_SIZE, (int)Math.floor(i / BOARD_SIZE), player)) {
						return false;
					}
				}
			}
		} 
		
		return true;
	}
	
	public boolean isAllowed(int x, int y, Player player, boolean exception) throws PositionTakenException, NotTouchingException, CapturingException {

		if (!exception)
			return this.isAllowed(x,  y,  player);
		
		// check if set is allowed (is it taken?)
		if (board[y][x] != null) {
			throw new PositionTakenException(x, y);
		}
		
		if (!isTouching(x, y)) {
			throw new NotTouchingException(x, y);
		}
		
		// is it capturing?
		boolean capturing = this.isCapturing(x, y, player);
		if (!capturing) {
			for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
				// Possible move?
				if (board[(int)Math.floor(i / BOARD_SIZE)][i % BOARD_SIZE] == null && isTouching(i % BOARD_SIZE, (int)Math.floor(i / BOARD_SIZE))) {
					// Capturing?
					if (this.isCapturing(i % BOARD_SIZE, (int)Math.floor(i / BOARD_SIZE), player)) {
						throw new CapturingException(x, y);
					}
				}
			}
		} 
		
		return true;
		
	}
	
	private void put(int x, int y, Player player) {
		
		board[y][x] = player;
		this.setChanged();
		this.notifyObservers(new Move(x, y, player));
		
	}
	
	private boolean isTouching(int x, int y) {
		// ^<
		if (x > 0 && y > 0 && !(board[y-1][x-1] == null))
			return true;
		
		// <
		if (x > 0 && !(board[y][x-1] == null))
			return true;
		
		// ^
		if (y > 0 && !(board[y-1][x] == null))
			return true;
		
		// ^>
		if (x < BOARD_SIZE-1 && y > 0 && !(board[y-1][x+1] == null))
			return true;
		
		// >
		if (x < BOARD_SIZE-1 && !(board[y][x+1] == null))
			return true;
		
		// V>
		if (x < BOARD_SIZE-1 && y < BOARD_SIZE-1 && !(board[y+1][x+1] == null))
			return true;
		
		// V
		if (y < BOARD_SIZE-1 && !(board[y+1][x] == null))
			return true;
		
		// V<
		if (x > 0 && y < BOARD_SIZE-1 && !(board[y+1][x-1] == null))
			return true;
		
		return false;
		
	}
	
	private boolean isCapturing(int x, int y, Player player) {

		// horizontal <x
		int i = Math.max(x-1, 0);
		while (i > 0 && !(board[y][i] == null) && !board[y][i].equals(player)) {
			i--;
		}
		if (player.equals(board[y][i]) && Math.abs(x-i) > 1) {
			return true;
		} 
		// horizontal >x
		i = Math.min(x+1, BOARD_SIZE-1);
		while (i < BOARD_SIZE-1 && !(board[y][i] == null) && !board[y][i].equals(player)) {
			i++;
		}
		if (player.equals(board[y][i]) && Math.abs(x-i) > 1) {
			return true;
		} 
		
		// vertical <y
		i = Math.max(y-1, 0);
		while (i > 0 && !(board[i][x] == null) && !board[i][x].equals(player)) {
			i--;
		}
		if (player.equals(board[i][x]) && Math.abs(y-i) > 1) {
			return true;
		} 
		
		// vertical >y
		i = Math.min(y+1, BOARD_SIZE - 1);
		while (i < BOARD_SIZE-1 && !(board[i][x] == null) && !board[i][x].equals(player)) {
			i++;
		}
		if (player.equals(board[i][x]) && Math.abs(y-i) > 1) {
			return true;
		}
		// diagonal <y <x
		i = Math.max(x-1, 0);
		int j = Math.max(y-1, 0);
		while (i > 0 && j > 0 && !(board[j][i] == null) && !board[j][i].equals(player)) {
			i--;
			j--;
		}
		if (player.equals(board[j][i]) && Math.abs(y-j) > 1) {
			return true;
		} 

		// diagonal >y >x
		i = Math.min(x+1, BOARD_SIZE - 1);
		j = Math.min(y+1, BOARD_SIZE - 1);
		while (i < BOARD_SIZE-1 && j < BOARD_SIZE-1 && !(board[j][i] == null) && !board[j][i].equals(player)) {
			i++;
			j++;
		}
		if (player.equals(board[j][i]) && Math.abs(y-j) > 1) {
			return true;
		} 

		// diagonal >y <x
		i = Math.max(x-1, 0);
		j = Math.min(y+1, BOARD_SIZE - 1);
		while (i > 0 && j < BOARD_SIZE-1 && !(board[j][i] == null) && !board[j][i].equals(player)) {
			i--;
			j++;
		}
		if (player.equals(board[j][i]) && Math.abs(y-j) > 1) {
			return true;
		} 

		// diagonal <y >x
		i = Math.min(x+1, BOARD_SIZE - 1);
		j = Math.max(y-1, 0);
		while (i < BOARD_SIZE-1 && j > 0 && !(board[j][i] == null) && !board[j][i].equals(player)) {
			i++;
			j--;
		}
		if (player.equals(board[j][i]) && Math.abs(y-j) > 1) {
			return true;
		} 
		
		// not capture and blocking
		return false;
		
	}
	public int[] getEmpty() {
		Set<Integer> res = new HashSet<Integer>();
		for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
			if (this.get(i) == null){
				res.add(i);
			}
		}
		
		int[] r = new int[res.size()];
		int i = 0;
		for (Integer iter : res) {
			r[i] = iter; 
			i++;
		}
		
		return r;
	}
	
	public boolean isFull(){
		for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
			if (this.board[(int)Math.floor(i / BOARD_SIZE)][i % BOARD_SIZE] == null)
				return false;
		}
		
		return true;
	}
	
	public String toString(){
		String temp = "";
		
		for(Player[] row : board){
			for(Player p : row){
				if(p != null)
					temp += (p.getPosition() + 1) + " ";
				else
					temp += "0 ";
			}
		}
		
		return temp.trim();
	}
	
	public int getPossession(Player player) {
		int res = 0;
		for (int j = 0; j < BOARD_SIZE; j++) {
			for (int i = 0; i < BOARD_SIZE; i++) {
				if (player.equals(this.board[j][i]))
					res++;
			}
		}
		
		return res;
	}

	public int[] getPossible(Player player) {
		Set<Integer> res = new HashSet<Integer>();
		for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
			if (this.isAllowed(i % BOARD_SIZE, (int)Math.floor(i / BOARD_SIZE), player))
				res.add(i);
		}
		
		int[] r = new int[res.size()];
		int i = 0;
		for (Integer iter : res) {
			r[i] = iter; 
			i++;
		}
		
		return r;
		
	}
	
	public static void main(String[] args) {
		final int size = BOARD_SIZE * BOARD_SIZE;
		
		Player p1 = new Player("Mudhabban"); // Indier
		Player p2 = new Player("Yildirhim"); // Turk
		Player p3 = new Player("Rodriquez"); // Mexicaan
		Player p4 = new Player("van der P"); // Nederlander
		
		Player temp = new Player("");
		
		Player[] players = new Player[]{p1, p2, p3, p4};
		
		Board board = new Board();
		board.init(players);
		
		/* Testing init() / get(int)
		 * expected output:
		 * 
		 	Mudhabban has placed succesfully
			Yildirhim has placed succesfully
			van der P has placed succesfully
			Rodriquez has placed succesfully
		 *
		 */
		
		System.out.println("\n\nTesting init() and get(int)\n=========================\n\n");
		
		for(int i = 0; i < size; i++){
			temp = board.get(i);
			if(temp != null){
				if(i == 27)
					System.out.println(temp == p1 ? p1.getName() + " has placed succesfully" : "Error placing " + p1.getName());
				if(i == 28)
					System.out.println(temp == p2 ? p2.getName() + " has placed succesfully" : "Error placing " + p2.getName());
				if(i == 36)
					System.out.println(temp == p3 ? p3.getName() + " has placed succesfully" : "Error placing " + p3.getName());
				if(i == 35)
					System.out.println(temp == p4 ? p4.getName() + " has placed succesfully" : "Error placing " + p4.getName());
			}
			
			if(i != 27 && i != 28 && i != 36 && i != 35 && temp != null)
				System.out.println(temp.getName() + " got misplaced");
		}
		
		/*
		 * Testing set(Move) (includes isAllowed() (includes isTouching() and isCapturing()))
		 * expected output:
		 * 
		 	PositionTakenException
			Move not made
			NotTouchingException
			Move not made
			CapturingException
			Move not made
			Move made
		 */
		
		System.out.println("\n\nTesting set(Move) (includes isAllowed() (includes isTouching() and isCapturing()))\n=========================\n\n");
		
		Move move = new Move(27, p2);
		try {
			board.set(move);
		} catch (PositionTakenException | NotTouchingException
				| CapturingException e) {
			System.out.println(e.getClass().getSimpleName());
		}
		System.out.println(board.get(27) == p1 ? "Move not made" : "Error: move made");
		
		move = new Move(1, p1);
		try {
			board.set(move);
		} catch (PositionTakenException | NotTouchingException
				| CapturingException e) {
			System.out.println(e.getClass().getSimpleName());
		}
		System.out.println(board.get(1) == null ? "Move not made" : "Error: move made");
		
		move = new Move(29, p2);
		try {
			board.set(move);
		} catch (PositionTakenException | NotTouchingException
				| CapturingException e) {
			System.out.println(e.getClass().getSimpleName());
		}
		System.out.println(board.get(29) == null ? "Move not made" : "Error: move made");
		
		move = new Move(29, p1);
		try {
			board.set(move);
		} catch (PositionTakenException | NotTouchingException
				| CapturingException e) {
			System.out.println(e.getClass().getSimpleName());
		}
		System.out.println(board.get(29) == p1 ? "Move made" : "Error: move not made");


		/*
		 * Testing getPossible(Player), getPossession(Player), getEmpty(), isFull()
		 * expected output:
		 	Done testing
		 */
		
		System.out.println("\n\nTesting getPossible(Player), getPossession(Player), getEmpty(), isFull()\n=========================\n\n");
		
		boolean toomany = false;
		int possibilities = 4;
		
		for(int i : board.getPossible(p1)){
			switch(i){
			case 42:
			case 43:
			case 44:
			case 45:
				possibilities--;
				break;
			default:
				toomany = true;
			}
		}
		
		if(toomany)
			System.out.println("Too many options");
		
		if(possibilities > 0)
			System.out.println("Forgotten options");
		
		if(board.getPossession(p1) != 3)
			System.out.println("Possession went wrong");
		
		if(board.isFull())
			System.out.println("Board appeared full");
		
		int empty = 59;
		
		for(int i : board.getEmpty()){
			switch(i){
			case 27:
			case 28:
			case 29:
			case 35:
			case 36:
				empty++; // Wrong empty should not be counted
				System.out.println("Non empty returned as empty");
			}
		}
		
		if(board.getEmpty().length < empty)
			System.out.println("Too little empties returned");
		
		if(board.isFull())
			System.out.println("Board falsely marked as full");
		
		// Filling it up!
		
		for(int i : board.getEmpty()){
			move = new Move(i,p1);
			board.put(move.getX(), move.getY(), p1);
		}
		
		if(board.getEmpty().length != 0)
			System.out.println("Still found an empty");
		
		if(!board.isFull())
			System.out.println("Board falsely marked as not full");
		
		System.out.println("Done testing");
		
		
		/*
		 * Testing deepCopy(), toString(), Board(String, Player[])
		 * expected output:
		 	Done testing
		 */
		
		System.out.println("\n\nTesting getPossible(Player), getPossession(Player), getEmpty(), isFull()\n=========================\n\n");
		
		Board b1 = board;
		Board b2 = b1.deepCopy();
		
		if(board != b1 || b1 == b2)
			System.out.println("Deep copy is worthless");
		
		if(!b1.toString().equals(b2.toString()))
			System.out.println("Deep copy did not produce identical board");

		b2 = new Board(b2.toString(), players);
		
		if(!b1.toString().equals(b2.toString()))
			System.out.println("Soft copy did not produce identical board");

		System.out.println("Done testing");
	}
	
}