package server;

import java.util.LinkedList;
import java.util.List;

import rolit.Board;
import rolit.CapturingException;
import rolit.Move;
import rolit.NotTouchingException;
import rolit.Player;
import rolit.PositionTakenException;

public class Game {
	
	private List<PlayerConnection> players;
	private Board board;
	private int maxPlayerNumber;
	
	private int turn = -1;
	
	public Game(int maxPlayerNumber){
		this(
				new Board(), 
				new LinkedList<PlayerConnection>()
		);
		
		this.maxPlayerNumber = maxPlayerNumber;
	}

	public Game(Board board, List<PlayerConnection> players){
		this.board = board;
		this.players = players;
	}
	
	public void endGame(boolean finished){
		int score;
		
		
		for(PlayerConnection p : players){
			score = 0;
			if(finished){
				int length = Board.BOARD_SIZE * Board.BOARD_SIZE;
				for(int i = 0; i < length; i++)
					if(p.getPlayer() == board.get(i))
						score++;
			}
			
			p.endGame(score);
		}
		
		if(Server.VERBOSE && finished) System.out.println("A game has finished");
	}
	
	public void kill(PlayerConnection pc){
		if(Server.VERBOSE) System.out.println(pc.getPlayer().getName() + " got killed. His game is ended");
		for(PlayerConnection p : players){
			if(p == pc){ // Same instance; no equals
				players.remove(pc);
				endGame(false);
				return;
			}
		}
	}
	
	public void sendChat(Player p, String message){
		for(PlayerConnection pc : this.players){
			pc.sendChat(String.format("%s %s", p.getName(), message));
		}
	}
	
	public void addPlayer(PlayerConnection pc){
		this.players.add(pc);
		
		if((maxPlayerNumber == 0 && this.players.size() >= 2) || (this.players.size() >= maxPlayerNumber && maxPlayerNumber >= 2)){
			maxPlayerNumber = players.size();
			startGame();
		}
	}

	public void makeMove(String move, Player player) throws PositionTakenException, NotTouchingException, CapturingException, NotYourTurnException{
		if(move.equals("")){
			System.out.println("ERROR: move is empty");
			return;
		}
		
		if(turn != player.getPosition()) throw new NotYourTurnException();
		
		String[] strMoves = move.split(" ");
		Move moove;
		
		int x = Integer.parseInt(strMoves[0]);
		int y = Integer.parseInt(strMoves[1]);
		
		moove = new Move(x, y, player);
		
		board.set(moove);
		
		for(PlayerConnection p : this.players){
			p.sendMove(moove);
		}
		
		this.nextTurn();
	}

	public int getMaxPlayerNumber() {
		return maxPlayerNumber;
	}
	
	public void setMaxPlayerNumber(int i){
		if(i == 0 || i == 2 || i == 3 || i == 4)
			maxPlayerNumber = i;
	}
	
	public Board getBoard(){
		return board;
	}
	
	public boolean isAwaiting(){
		return (turn == -1);
	}

	public List<PlayerConnection> getPlayers() {
		return players;
	}
	
	private void startGame(){
		Player[] playerArray = new Player[maxPlayerNumber];
		for(int i = 0; i < maxPlayerNumber; i++)
			playerArray[i] = players.get(i).getPlayer();
		
		board.init(playerArray);
		
		String command = "";
	
		int position = 0;
		for(PlayerConnection p : this.players){
			p.getPlayer().setPosition(position++);
			command += p.getPlayer().getName() + " ";
		}
		
		for(PlayerConnection p : this.players){
			p.startGame(command.trim());
		}
		
		nextTurn();
		
		if(Server.VERBOSE) System.out.println("A new game has started with " + command);
		
		
	}
	
	private void nextTurn(){
		if(board.isFull()){
			endGame(true);
			return;
		}

		this.turn = (this.turn + 1) % maxPlayerNumber;

		for(PlayerConnection p : this.players){
			p.nextTurn(this.turn);
		}
	}
}
