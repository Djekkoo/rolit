package server;

public class NotYourTurnException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3695723857544494710L;

	public NotYourTurnException(){
		super("It is not your turn!");
	}
}
