/**
 * 
 */
package server;

import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import rolit.Player;

/**
 * @author sander, jacco
 *
 */
public class Leaderboard {
	
	private ArrayList<Score> scores = new ArrayList<Score>();
	
	public Leaderboard(){
		Toolkit.getDefaultToolkit().beep();
	}
	
	/**
	 * Adds a score to the List
	 * 
	 * @param Number instance representing the score
	 * @param Timestamp at which it was scored
	 * @param The player that scored it
	 */
	
	public void addScore(Number score, Calendar time, Player player){
		Score tempScore = new Score(score, player, time);
		scores.add(tempScore);
		
		Collections.sort(scores);
	}
	
	public void addScore(Number score, Player player){	addScore(score, Calendar.getInstance(),player);	}
	
	/**
	 * Method that returns the best # scores
	 *  
	 * @param Optional: the number of top scores (default is 3)
	 * @param Optional: a list of scores to measure in
	 * @return A list of scores
	 */
	
	public List<Score> getTop(int top, List<Score> scores){
		int size = scores.size();
		int n = Math.min(top, size);
		
		return scores.subList(size-n, size);
	}

	public List<Score> getTop(int top){					return getTop(top, this.scores);	}

	public List<Score> getTop(List<Score> scores){		return getTop(3, scores);			}
	
	public List<Score> getTop(){						return getTop(3);					}
	
	/**
	 * Method that returns a list of scores above a certain score
	 * 
	 * @param Number instance representing the score to measure against
	 * @param Optional: list of scores to measure in
	 * @return A list of scores
	 */
	
	public List<Score> getAbove(Number score, List<Score> scores){
		Score n = new Score(score, null);
		List<Score> temp = new ArrayList<Score>();
		
		for(Score i : scores){
			if(i.compareTo(n) > 0){
				temp.add(i);
			}
		}
		
		return temp;
	}
	
	public List<Score> getAbove(Number score){	return getAbove(score, this.scores);	}

	/**
	 * Method that returns a list of scores above a certain score
	 * 
	 * @param Number instance representing the score to measure against
	 * @param Optional: list of scores to measure in
	 * @return A list of scores
	 */
	
	public List<Score> getBelow(Number score, List<Score> scores){
		Score n = new Score(score, null);
		List<Score> temp = new ArrayList<Score>();
		
		for(Score i : scores){
			if(i.compareTo(n) < 0){
				temp.add(i);
			}
		}
		
		return temp;
	}
	
	public List<Score> getBelow(Number score){	return getBelow(score, this.scores);	}
	
	/**
	 * Method that gets the average of a list of scores
	 * 
	 * @param Optional: a list of scores to measure in
	 * @return A double representing the average
	 */
	
	public double getAverageOverall(List<Score> scores){
		double temp = 0;
		
		for(Score i : scores){
			temp += i.getScore().doubleValue();
		}
		
		return temp/scores.size();
	}
	
	public double getAverageOverall(){return getAverageOverall(this.scores);}
	
	/**
	 * Method that calculates the average score in a given month
	 * 
	 * @param Optional: a calendar to measure against
	 * @param Optional: a list of scores to measure in
	 * @return the average of the month
	 */
	
	public double getAverageOfMonth(Calendar cal, List<Score> scores){
		double temp = 0;
		int n = 0;
		
		for(Score i : scores){
			if(i.getTime().get(Calendar.MONTH) == cal.get(Calendar.MONTH) && 
					i.getTime().get(Calendar.YEAR) == cal.get(Calendar.YEAR)){
				temp += i.getScore().doubleValue();
				n++;
			}
		}
		
		return temp/n;
	}
	
	public double getAverageOfMonth(List<Score> scores){return getAverageOfMonth(Calendar.getInstance(), scores);	}
	
	public double getAverageOfMonth(Calendar cal){		return getAverageOfMonth(cal, this.scores);					}

	public double getAverageOfMonth(){					return getAverageOfMonth(Calendar.getInstance());			}
	
	/**
	 * Method that calculates the average score on a given day
	 * 
	 * @param Optional: a calendar to measure against
	 * @param Optional: a list of scores to measure in
	 * @return the average of the month
	 */
	
	public double getAverageOfDay(Calendar cal, List<Score> scores){
		double temp = 0;
		int n = 0;
		
		for(Score i : scores){
			if(i.getTime().get(Calendar.MONTH) == cal.get(Calendar.MONTH) && 
					i.getTime().get(Calendar.YEAR) == cal.get(Calendar.YEAR) &&
					i.getTime().get(Calendar.DATE) == cal.get(Calendar.DATE)){
				temp += i.getScore().doubleValue();
				n++;
			}
		}
		
		return temp/n;
	}

	public double getAverageOfDay(Calendar cal){		return getAverageOfDay(cal, this.scores);				}
	
	public double getAverageOfDay(List<Score> scores){	return getAverageOfDay(Calendar.getInstance(), scores);	}
	
	public double getAverageOfDay(){					return getAverageOfDay(Calendar.getInstance());			}
	
	/**
	 * Method that extracts the scores a player made from the list of scores
	 * 
	 * @param the player who made the scores
	 * @return a list of scores
	 */
	
	
	public List<Score> getPlayerScores(Player player){
		List<Score> temp = new ArrayList<Score>();
		
		for(Score i : scores){
			if(i.getPlayer().equals(player)){
				temp.add(i);
			}
		}
		
		return temp;
	}
	
	/**
	 * Method that calculates the average of a specific player
	 * 
	 * @param the player who made the scores
	 * @return a double representing the average
	 */
	
	public double getPlayerAverage(Player player){
		return getAverageOverall(getPlayerScores(player));
	}
	
	/**
	 * Method that returns the day on which the best average was scored by a specific player
	 * 
	 * @param the player who made the scores
	 * @return a calendar object representing that
	 */
	
	public Calendar getDayOfBestResults(Player player){
		List<Score> scores = getPlayerScores(player);
		Map<Calendar,double[]> scoremap = new HashMap<Calendar,double[]>();
		
		double[] t = null;
		boolean found;
		Calendar cal;
		
		for(Score i : scores){
			cal = i.getTime();
			found = false;
			t = new double[]{0,0};
			for(Calendar c : scoremap.keySet()){
				if(cal.get(Calendar.YEAR) == c.get(Calendar.YEAR) && 
						cal.get(Calendar.MONTH) == c.get(Calendar.MONTH) &&
						cal.get(Calendar.DATE) == c.get(Calendar.DATE)){
					t = scoremap.get(c);
					t[0] += i.getScore().doubleValue();
					t[1]++;
					scoremap.put(c, t);
					found = true;
				}
			}
			if(!found){
				t[0] = i.getScore().doubleValue();
				t[1] = 1;
				scoremap.put(cal, t);
				found = false;
			}
		}

		Map<Double,Calendar> avgmap = new HashMap<Double,Calendar>();
		Double d = null;
		
		for(Calendar c : scoremap.keySet()){
			t = scoremap.get(c);
			d = t[0]/t[1];
			avgmap.put(d, c);
		}
		
		cal = avgmap.get(Collections.max(avgmap.keySet()));
		
		return cal;
	}
	

	public class Score implements Comparable<Score>{
		private final Number score;
		private final Calendar time;
		private final Player player;
		
		public Score(Number score, Player player, Calendar time){
			this.score = score;
			this.time = time;
			this.player = player;
		}
		
		public Score(Number score, Player player){
			this.score = score;
			this.time = Calendar.getInstance();
			this.player = player;
		}
		
		public Number getScore(){
			return score;
		}
		
		public Calendar getTime(){
			return time;
		}
		
		public Player getPlayer(){
			return player;
		}
		
		@Override
		public int compareTo(Score arg0) {
			double x = score.doubleValue() - arg0.getScore().doubleValue();
			if(x < 0){
				return -1;
			} else if (x > 0) {
				return 1;
			}
			
			return 0;
			
		}
		
		public String toString(){
			return String.format("Score %s made on %s-%s by %s", 
					score, 
					time.get(Calendar.DAY_OF_MONTH),
					time.getDisplayName(Calendar.MONTH, Calendar.SHORT,new Locale("en")), 
					player);
		}
	}
	
	public static void main(String[] args) {
		Leaderboard leaderboard = new Leaderboard();
		Player x = new Player(null, 0);
		Player y = new Player(null, 0);
		Calendar tomorrow = Calendar.getInstance();
		tomorrow.add(Calendar.DATE, 1);
		
		Calendar nextMonth = Calendar.getInstance();
		nextMonth.add(Calendar.MONTH, 1);

		leaderboard.addScore(100, x);
		leaderboard.addScore(50, tomorrow, x);
		leaderboard.addScore(75, tomorrow, x);
		leaderboard.addScore(125, nextMonth, x);
		leaderboard.addScore(25, nextMonth, x);

		System.out.println(leaderboard.getAverageOverall());
		System.out.println(leaderboard.getAverageOfMonth());
		System.out.println(leaderboard.getAverageOfDay());
		
		
		System.out.println(leaderboard.getBelow(75.0));
		System.out.println(leaderboard.getAbove(75.0));
		System.out.println(leaderboard.getTop());
		
		leaderboard.addScore(75, y);

		System.out.println(leaderboard.getPlayerAverage(x));
		System.out.println(leaderboard.getPlayerAverage(y));
		
		Calendar cal = leaderboard.getDayOfBestResults(x);
		System.out.println(String.format("%s-%s", 
				cal.get(Calendar.DATE),
				cal.getDisplayName(Calendar.MONTH, Calendar.SHORT,new Locale("en"))
				));
		
	}
}
