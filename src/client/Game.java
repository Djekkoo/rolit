package client;

import rolit.Board;
import rolit.Callback;
import rolit.CapturingException;
import rolit.Move;
import rolit.NotTouchingException;
import rolit.Player;
import rolit.PositionTakenException;
import rolit.Protocol;

public class Game {
	
	private Board b;
	private Player[] players;
	private int player;
	private boolean sleep;
	private GUI gui;
	private Client c;

	public Game(Player[] players, Board b, Client c) {
		this.c = c;
		this.players = players;
		this.sleep = false;
		
		this.gui = new GUI(new Callback(this, "set"), new Callback(c, "chat"));
		this.b = b;
		this.b.addObserver(this.gui);
		this.b.init(players);

		setTurn(0);
		
	}
	
	public Game(Player[] players, Client c) {

		this(players, new Board(), c);

	}
	
	public void wakeup() {
		this.sleep = false;
	}

	// F2: set(Integer position, Player player): called by server, does set, checks if client is at turn
	//				if so: check if AI should work, else wait until set(Integer position) is called (just return;)
	
	public void set(Integer position, Player player) {
		
		try {
			
			b.set(position, player);
			setTurn((player.getPosition() + 1) % this.players.length);
			
		} catch (PositionTakenException | NotTouchingException | CapturingException e) {
			System.out.println("Error in board: " + e.getMessage());
			if (c != null){
				c.getConnection().send(Protocol.make(Protocol.BOARD));
			}
		}
	}

	// F1: set(Integer position): called on button click, send to server&parse locally
	// Also called by the AI as feedback
	public void set(Integer position) {
		
		// If it's not your turn, return to where you came from!
		if(!isMe()) return;
		
		// If you are AI, let AI do the work!
		if(sleep && this.players[getTurn()].isAI()) return;
		
		try {
			this.b.set(position, this.players[getTurn()]);
			setTurn((getTurn() + 1) % this.players.length);
		} catch (Exception e) {
		}
		
		c.getConnection().send(
				Protocol.make(Protocol.GMOVE, String.format("%s %s", position % Board.BOARD_SIZE, (int)Math.floor(position / Board.BOARD_SIZE))));
		
	}
	
	public boolean isMe(){
		return isMe(player);
	}
	
	public boolean isMe(int player){
		return this.players[player].isClient();
	}
	
	public Board getBoard(){
		return this.b;
	}
	
	public void setBoard(String message){
		
		this.b = new Board();
		this.b.addObserver(this.gui);
		this.b.make(message, players);
	}

	public int getTurn() {
		return player;
	}

	public void setTurn(int player) {
		this.player = player;
		
		// highlight possibilities
			// opponents
		Player[] opponents = new Player[this.players.length - 1];
		int i = 0;
		for (Player p : this.players) {
			if (! (p.getPosition() == player)) {
				opponents[i] = p;
				i++;
			}
		}
		
		Move move = ComputerPlayer.getMove(this.b, this.players[player], opponents);
		if (isMe()) {
			this.gui.highlight(move.getI(), this.b.getPossible(this.players[player]), this.b.getEmpty());
		} else {
			this.gui.highlight(-1, new int[0], this.b.getEmpty());
		}
		
		// AI?
		if (isMe() && this.players[getTurn()].isAI() && move != null) {
			this.sleep = true;
			new Thread(new AIMove(move, new Callback(this, "wakeup"), new Callback(this, "set"))).start();
		}
		
	}
	
	public void makeSet(String message){
		String[] strMove = message.split(" ");
		int x = Integer.parseInt(strMove[1]);
		int y = Integer.parseInt(strMove[2]);
		int p = Integer.parseInt(strMove[0]) - 1;
		int i = (y * Board.BOARD_SIZE) + x;
		
		if(isMe(p)) return;
		
		Player player = null;
		
		for(Player pp : players){
			if(p == pp.getPosition())
				player = pp;
		}
		
		set(i, player);
		
	}
	
	public void appendChat(String name, String message){
		gui.appendChat(name, message);
		
	}
	
	public void close() {
		this.gui.close();
		this.gui = null;
	}
	
}
