package server;

public class PlayerIsPlayingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4268669443732308806L;

	public PlayerIsPlayingException(){
		super("The player is already playing!");
	}
}
