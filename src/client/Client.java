package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.apache.commons.codec.binary.Base64;

import rolit.Callback;
import rolit.Connection;
import rolit.Player;
import rolit.Protocol;
import rolit.Protocol.STATES;

public class Client {

	// For the PKI server:
	public static final String SERVER_ADDRESS = "ss-security.student.utwente.nl";
	public static final int SERVER_PORT = 2013;
	
	public static final boolean chatsupport = true;
	public static final boolean lobbysupport = false;
	
	public boolean usechat;
	public boolean uselobby;

	private Game game;
	private String player;
	private String password;
	private ServerConnection connection;
	private LoginGUI lGUI;
	private boolean waiting;
	private String host;
	private Integer port;
	private Boolean ai;
	private Protocol.STATES status;
	
	private String gameType = "H";
	
	public Client() throws UnknownHostException, IOException{
		
		//Player PJ = new Player("Ja", 0, 0);
		//PJ.setAI(true);
		//this.game = new Game(new Player[] {new Player("Sa", 1, 0), PJ}, this);

		this.lGUI = new LoginGUI(new Callback(this, "filled"));
		waitForInput();
		
	}
	
	public void chat(String message) {
		if (this.connection != null)
			this.connection.send(
					Protocol.make(Protocol.CHATM, message));
	}
	
	private void waitForInput() {
		this.waiting = false;
		do {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {}
			
		} while (this.waiting == false);
		this.startConnection();
	}
	
	public void filled(String username, String password, String host, Integer port, Boolean ai, String type) {
		this.player = username;
		this.password = password;
		this.host = host;
		this.port = port;
		this.ai = ai;
		this.waiting = true;

		if ("2 Players".equals(type)) {
			this.gameType = "H";
		}
		else if ("3 Players".equals(type)) {
			this.gameType = "I";
		} else if ("4 Players".equals(type)) {
			this.gameType = "J";
		} else if ("Default game".equals(type)) {
			this.gameType = "D";
		}
		
	}
	
	public void connectionLost() {
		
		if (this.game != null)
			this.game.close();
		
		this.connection.kill();
		
		this.connection = null;
		this.player = null;
		this.password = null;
		this.host = null;
		this.game = null;
		
		this.lGUI.open();
		this.lGUI.showPopup("Error", "Server has disconnected");
		this.waitForInput();
		return;
		
	}
	
	public void startConnection() {

		Socket socket;
		try {
			socket = new Socket(this.host, this.port);
		} catch (IOException e) {
			this.lGUI.showPopup("Error", "Could not connect to host");
			
			this.waitForInput();
			return;
		}
		
		this.lGUI.close();
		
		this.connection = new ServerConnection(socket, new Callback(this, "incoming"), new Callback(this, "connectionLost"));
		this.connection.send(
				Protocol.make(Protocol.LOGIN, this.player));
		new Thread(this.connection).start();
		
		String input = readString();
		while(!(input == null || input.equals("exit"))){
			connection.send(input);
			input = readString();
		}
		
		
	}
	
	public static void main(String[] args) throws IOException {

		new Client();
		
	}
	
	// happens in another thread
	public void incoming(Protocol message) {
		
		switch (message.getCommand()) {
		case Protocol.VSIGN:
			try {
				String text = message.getText();
				String signature = sign(text, retrievePrivateKey());
				
				connection.send(
						Protocol.make(Protocol.VSIGN, signature));
				
			} catch (InvalidKeyException | NoSuchAlgorithmException
					| SignatureException | InvalidKeySpecException
					| IOException e) {
				connectionLost();
			}
			break;
			
		case Protocol.HELLO:
			usechat = message.supportChat() && chatsupport;
			uselobby = message.supportLobby() && lobbysupport;
			
			String flags = "";
			if(chatsupport) flags = "C";
			if(lobbysupport) flags += "L";
			if(flags.equals("")) flags = "D";
			
			connection.send(
					Protocol.make(Protocol.HELLO, null, flags));
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {}
			
			connection.send(
					Protocol.make(Protocol.NGAME, null, this.gameType));
			break;
			
		case Protocol.START:
			ArrayList<Player> players = new ArrayList<Player>();
			Player[] temp = new Player[0];
			
			int i = 0, j = 0;
			String name = message.getName();
			
			while(name != null && !name.equals("")){
				if(name.equals(this.player)) j = i;
				
				players.add(new Player(name,i++));
				
				// Extract the next name
				name = message.getName();
			}
			
			temp = new Player[players.size()];
			
			for(i = 0; i < players.size(); i++){
				temp[i] = players.get(i);
				temp[i].setBasePosition(j);
				System.out.println("Player " + temp[i].getName() + " on position " + temp[i].getPosition() + " has been assigned base_position: " + j);
			}
			
			if (this.ai)
				temp[j].setAI(true);
			
			this.game = new Game(temp, this);
			
			//connection.send(Protocol.make(Protocol.BOARD));
			
			break;
			
		case Protocol.GTURN:
			if(game.getTurn() != message.getTurn() - 1){
				System.out.println("Turns did not match!");
				game.setTurn(message.getTurn() - 1);
			}
			break;
			
		case Protocol.GMOVE:
			game.makeSet(message.getText());
			break;
			
		case Protocol.BOARD:
			if(!game.getBoard().toString().equals(message.getText())){
				System.out.println("Boards aren't synchronized!");
				game.setBoard(message.getText());
			}
			break;
			
		case Protocol.ERROR:
			System.out.println(message.getText());
			break;
			
		case Protocol.STATE:
			this.status = message.getState();
			if(status == STATES.STOPPED){
				game.close();
				
				if(JOptionPane.showConfirmDialog(null, "Would you like to start a new game?", "New game", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)
					connection.send(
							Protocol.make(Protocol.NGAME, null, "H"));
					
			}
			break;
			
		case Protocol.SCORE:
			JOptionPane.showMessageDialog(null, "You scored : " + message.getText(), "Score", JOptionPane.OK_OPTION);
			break;
			
		case Protocol.CHATM:
			int y = message.getText().indexOf(" ");
			if(y != -1) ;
				game.appendChat(message.getText().substring(0,y), message.getText().substring(y + 1));
			break;
		}
	}
	
	public ServerConnection getConnection(){
		return this.connection;
	}
	
	private String sign(String message, PrivateKey pk) throws NoSuchAlgorithmException, InvalidKeyException, SignatureException{
		byte[] rawMessage = message.getBytes();
		Signature sig = Signature.getInstance("SHA1withRSA");
		sig.initSign(pk);
		sig.update(rawMessage);
		byte[] signature = sig.sign();
		
		return Base64.encodeBase64String(signature);
	}
	
	private PrivateKey retrievePrivateKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException{
		// Connect to PKI server
		Socket sock = new Socket(SERVER_ADDRESS, SERVER_PORT);
		Connection PKI = new Connection(sock);
		
		// Request
		PKI.send("IDPLAYER player_" + player + " " + password);
		String ans = PKI.receive();
		
		// Validate reply
		if(!( ans.length() > 8 && ans.substring(0, 7).equals("PRIVKEY") )) throw new IOException("Unexpected response");
		
		// Generate private key from reply
		PrivateKey pk = generatePrivateKey(ans.substring(8));
		
		return pk;
	}
	
	private PrivateKey generatePrivateKey(String string) throws NoSuchAlgorithmException, InvalidKeySpecException{
		byte[] rawKey = Base64.decodeBase64(string);
	
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(rawKey);
		KeyFactory fact = KeyFactory.getInstance("RSA");
		return fact.generatePrivate(keySpec);
	}	
	
	static public String readString() {
		String antw = null;
		
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			antw = in.readLine();
		} catch (IOException e) {}

		return (antw == null) ? "" : antw;
	}
	
}
