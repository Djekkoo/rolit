package client;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import rolit.Board;
import rolit.CapturingException;
import rolit.Move;
import rolit.NotTouchingException;
import rolit.Player;
import rolit.PositionTakenException;

public class ComputerPlayer {

	private static final int CORNER_SCORE = 100;
	private static final int BORDER_SCORE = 30;
	private static final int NORMAL_SCORE = 5;
	private static final int CORNER_PANELTY = -50;
	private static final int SAFE_CORNER_SCORE = 0;
	
	public static Move getMove(Board board, Player player, Player[] opponents) {
		
		int bValue = 0;
		List<Move> res = new LinkedList<Move>();
		
		// build an array of possible moves
		List<Move> possible = new LinkedList<Move>();
		for (int i = 0; i < Board.BOARD_SIZE * Board.BOARD_SIZE; i++) {
			if (board.isAllowed(i % Board.BOARD_SIZE, (int)Math.floor(i / Board.BOARD_SIZE), player)) {
				possible.add(new Move(i, player));
			}
		}
		
		if (possible.size() == 1)
			return possible.get(0);
		
		if (possible.size() == 0)
			return null;
		
		Iterator<Move> i = possible.iterator();
		int val;
		while (i.hasNext()) {
			Move move = i.next();
			val = getValue(move, board, player, opponents);
			if (val > bValue) {
				res = new LinkedList<Move>();
				res.add(move);
				bValue = val;
			}
			else if(val == bValue) {
				res.add(move);
			}
		}
		
		if (res.size() > 0) {
			
			int r = (int) Math.round(Math.random() * (res.size() - 1));
			Move f = res.get(r);
			
			System.out.println("======================================");
			System.out.println(r + "[" +f.getX()+ ","+f.getY()+"] "+bValue);
			
			return f;
			
		}
		
		int r = (int) Math.round(Math.random() * (possible.size() - 1));
		return possible.get(r);
		
	}

	private static int getValue(Move move, Board board, Player player, Player[] opponents) {

		int value = 0;
		Board nBoard = board.deepCopy();
		
		// corners?
		if (move.getX() == 0 && move.getY() == 0)
			value += CORNER_SCORE;
		
		if (move.getX() == Board.BOARD_SIZE-1 && move.getY() == 0)
			value += CORNER_SCORE;
		
		if (move.getX() == 0 && move.getY() == Board.BOARD_SIZE-1)
			value += CORNER_SCORE;
		
		if (move.getX() == Board.BOARD_SIZE-1 && move.getY() == Board.BOARD_SIZE-1)
			value += CORNER_SCORE;
		
		// taking borders?
			// <> Border
		for (int x = 0; x < Board.BOARD_SIZE; x+= Board.BOARD_SIZE - 1) {
			if (move.getX() == x && (player.equals(nBoard.get(x, 0)) || player.equals(nBoard.get(x, Board.BOARD_SIZE-1)))) {
			
				int win = 0;
				int i;
				if (player.equals(nBoard.get(x, Board.BOARD_SIZE-1))) {
					i = move.getY();
					while (!player.equals(nBoard.get(x, i)) && i < Board.BOARD_SIZE-1) {
						i++;
					}
					if (i == Board.BOARD_SIZE-1) 
						win += Board.BOARD_SIZE-1 - i - 1;
				}
				if (player.equals(nBoard.get(x, 0))) {
					i = 0;
					while (!player.equals(nBoard.get(x, i)) && i <  move.getY()) {
						i++;
					}
					if (i ==  move.getY()) 
						win +=  move.getY() - i - 1;
				}
				
				value += win*(BORDER_SCORE - NORMAL_SCORE);
				
			}
		}
			// ^V Border
		for (int y = 0; y < Board.BOARD_SIZE; y += Board.BOARD_SIZE - 1) {
			if (move.getY() == y && (player.equals(nBoard.get(0, y)) || player.equals(nBoard.get(Board.BOARD_SIZE-1, y)))) {
				
				int win = 0;
				int i;
				if (player.equals(nBoard.get(Board.BOARD_SIZE-1, y))) {
					i = move.getX();
					while (!player.equals(nBoard.get(i, y)) && i < Board.BOARD_SIZE-1) {
						i++;
					}
					if (i == Board.BOARD_SIZE-1) 
						win += Board.BOARD_SIZE-1 - i;
				}
				if (player.equals(nBoard.get(0, y))) {
					i = 0;
					while (!player.equals(nBoard.get(i, y)) && i <  move.getX()) {
						i++;
					}
					if (i ==  move.getX()) 
						win +=  move.getX() - i;
				}
				
				value += win*(BORDER_SCORE - NORMAL_SCORE);
				
			}
		}
		
		// How much does it capture?
		int current = nBoard.getPossession(player);
		try {
			
			Board test = nBoard.deepCopy();
			test.set(move);
			value += (test.getPossession(player) - current) * NORMAL_SCORE;
			
		} catch (PositionTakenException | NotTouchingException | CapturingException e) {}
		
		// panelties?
			// Leading to opponent capturing corner?
				// ^<
		int[] pos = new int[]{move.getX(), move.getY()};
		if (	nBoard.get(0, 0) == null && 
				(
					(pos[1] == 0 && pos[0] == 1) || 
					(pos[1] == 1 && pos[0] == 0) || 
					(pos[1] == 1 && pos[0] == 1)
				)
			) {
			value += CORNER_PANELTY;
			// uncapturable?
			Board test = nBoard.deepCopy();
			boolean losing = false;
			try {
				test.set(move);
			} catch (Exception e) { 
				losing = true;
			}
			for (Player opponent : opponents) {
				System.out.println(test.toString());
				System.out.println(pos[0] + "," + pos[1] + "--" + test.isAllowed(0, 0, opponent) + "--" + opponent.getName());
				try {
					if (test.isAllowed(0, 0, opponent, true)) {
				
					losing = true;
					}
				} catch(Exception e) {
					System.out.println(e.getClass() +"==" + e.getMessage());
				}
			}
			
			if (!losing) {
				value += SAFE_CORNER_SCORE;
			}
				
		}
				// ^>
		if (	nBoard.get(0, Board.BOARD_SIZE-1) == null && 
				(
					(pos[1] == 0 && pos[0] == Board.BOARD_SIZE-2) ||
					(pos[1] == 1 && pos[0] == Board.BOARD_SIZE-1) || 
					(pos[1] == 1 && pos[0] == Board.BOARD_SIZE-2)
				)
			) {
			value += CORNER_PANELTY;
			// uncapturable?
			Board test = nBoard.deepCopy();
			boolean losing = false;
			try {
				test.set(move);
			} catch (Exception e) { 
				losing = true;
			}
			for (Player opponent : opponents) {
				if (test.isAllowed(0, Board.BOARD_SIZE-1, opponent)) {
					losing = true;
				}
			}
			
			if (!losing) {
				value += SAFE_CORNER_SCORE;
			}
		}
				// V>
		if (	nBoard.get(Board.BOARD_SIZE-1, Board.BOARD_SIZE-1) == null && 
				(
					(pos[1] == Board.BOARD_SIZE-2 && pos[0] == Board.BOARD_SIZE-1) || 
					(pos[1] == Board.BOARD_SIZE-1 && pos[0] == Board.BOARD_SIZE-2) || 
					(pos[1] == Board.BOARD_SIZE-2 && pos[0] == Board.BOARD_SIZE-2)
				)
			) {
			value += CORNER_PANELTY;
			// uncapturable?
			Board test = nBoard.deepCopy();
			boolean losing = false;
			try {
				test.set(move);
			} catch (Exception e) { 
				losing = true;
			}
			for (Player opponent : opponents) {
				if (test.isAllowed(Board.BOARD_SIZE-1, Board.BOARD_SIZE-1, opponent)) {
					losing = true;
				}
			}
			
			if (!losing) {
				value += SAFE_CORNER_SCORE;
			}
		}
				//  V<
		if (	nBoard.get(Board.BOARD_SIZE-1, 0) == null && 
				(
					(pos[1] == Board.BOARD_SIZE-2 && pos[0] == 0) || 
					(pos[1] == Board.BOARD_SIZE-1 && pos[0] == 1) || 
					(pos[1] == Board.BOARD_SIZE-2 && pos[0] == 1)
				)
			) {
			value += CORNER_PANELTY;
			// uncapturable?
			Board test = nBoard.deepCopy();
			boolean losing = false;
			try {
				test.set(move);
			} catch (Exception e) { 
				losing = true;
			}
			for (Player opponent : opponents) {
				if (test.isAllowed(Board.BOARD_SIZE-1, 0, opponent)) {
					losing = true;
				}
			}
			
			if (!losing) {
				value += SAFE_CORNER_SCORE;
			}
		}
		
		System.out.println("[" +pos[0]+ ","+pos[1]+"] "+value);
		
		return value;
	}
	
}
