package server;

public class PlayerNotOnlineException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8270669665808395714L;
	
	public PlayerNotOnlineException(){
		super("Player is not online!");
	}

}
