package rolit;

public class CapturingException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3938767569628668256L;

	public CapturingException(int x, int y) {
		super("Position: ["+x+","+y+"] is not capturing and blocking, while another position is!");
	}
	
}