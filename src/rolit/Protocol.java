package rolit;

import java.text.ParseException;
/**
 * Object representing a certain command in the protocol. 
 * 
 * @see Protocol.parse
 * @author Sander, Jacco
 */
public class Protocol {
	public static final int LOGIN = 1;
	public static final int VSIGN = 2;
	public static final int HELLO = 3;
	public static final int STATE = 4;
	public static final int NGAME = 5;
	public static final int START = 6;
	public static final int GTURN = 7;
	public static final int GMOVE = 8;
	public static final int BOARD = 9;
	public static final int GPLST = 16;
	public static final int BCAST = 10;
	public static final int ERROR = 11;
	public static final int WARNI = 12;
	public static final int PROTO = 13;
	public static final int SINFO = 14;
	public static final int ALIVE = 17;
	public static final int SCORE = 15;
	public static final int PLIST = 18;
	public static final int LJOIN = 19;
	public static final int LEAVE = 20;
	public static final int INVIT = 21;
	public static final int CHATM = 22;
	
	public static enum STATES{
		WAITING, UNKNOWN, LOBBY, STARTING, PLAYING, STOPPED;
	}
	
	private int command;
	private String flags;
	private String info;
	
	private Protocol(int command){
		this(command, "", "");
	}
	
	private Protocol(int command, String info){
		this(command,info,"");
	}
	
	private Protocol(int command, String info, String flags){
		this.command = command;
		this.info = info;
		this.flags = flags;
	}
	
	/**
	 * Returns an integer representing the command extracted.
	 * Comparable to a list of public static integers.
	 */
	
	//@ ensures \result >= 1;
	//@ ensures \result <= 22;
	//@ pure;
	public int getCommand(){
		return command;
	}
	
	/**
	 * Method for extracting a state
	 * @return A STATES instance representing the extracted state
	 */
	//@ ensures \result instanceof STATES;
	//@ pure;
	public STATES getState(){
		try{
			return STATES.valueOf(flags);
		} catch (IllegalArgumentException e) {
			return STATES.UNKNOWN;
		}
	}
	
	/**
	 * Checks if chatsupport was enabled in the flags
	 * @return true if the flag C was set, otherwise false
	 */
	//@ pure;
	public boolean supportChat(){
		if(command == HELLO)	return flags.contains("C");
		else 					return false;
	}
	
	/**
	 * Checks if chatsupport was enabled in the flags
	 * @return true if the flag L was set, otherwise false
	 */
	//@ pure;
	public boolean supportLobby(){
		if(command == HELLO)	return flags.contains("L");
		else 					return false;
	}
	
	/**
	 * Method for extracting a turn
	 * @return An Integer representation of the flags
	 */
	//@ pure;
	public int getTurn(){
		if(command == GTURN)	return Integer.parseInt(info);
		else 					return 0;
	}
	
	/**
	 * Gets the text that was given with the command, if any.
	 * @return for most commands a String instance of the commandtext
	 */
	//@ pure;
	public String getText(){
		switch(command){	
		case VSIGN:
		case BCAST:
		case GMOVE:
		case ERROR:
		case WARNI:
		case BOARD:
		case CHATM:
			return info;
		case NGAME:
		case SCORE:
			return flags;
		default:
			return null;
		}	
	}
	
	/**
	 * Convenience method for extracting a name from the commandtext. 
	 * If more than one name found getName() will return a different name every time it is called.
	 * 
	 * @return a trimmed String instance representing one playername
	 */
	//@ pure;
	public String getName(){
		switch(command){
		case LOGIN:
		case GTURN:
		case LJOIN:
		case LEAVE:
			return info;
		case NGAME:
			if(flags.equals("CL")) return info;
			else return null;
		case START:
		case GPLST:
		case PLIST:
			int x = info.indexOf(" ");
			if(x == -1) {
				String temp = info;
				info = "";
				return temp;
			} else {
				String temp = info.substring(0, x);
				info = info.substring(x + 1);
				return temp;
			}
		case GMOVE:
			return info.substring(0, 1);
		case PROTO:
		case SINFO:
			return info.split(" ")[0];
		case ERROR:
			return flags;
		case INVIT:
			if(!flags.equals("R")){ return null; }
			int y = info.indexOf(" ");
			if(y == -1) {
				String temp = info;
				info = "";
				return temp;
			} else {
				String temp = info.substring(0, y);
				info = info.substring(y + 1);
				return temp;
			}
		default:
			return null;
		}
	}
	/**
	 * For the SINFO and PROTO command it returns the raw version
	 * @return A String in the format #.#.# where # is an integer
	 */
	//@ pure;
	public String getVersion(){
		switch(command){
		case SINFO:
		case PROTO:
			return info.split(" ")[1];
		default:
			return null;
		}
	}
	
	/**
	 * Clever method that generates a message to send over the network. Will always return a Protocol supported command
	 * 
	 * @param c an integer representing a command
	 * @return a String compliant with the protocol
	 */
	//@ requires c >= 1 && c <= 22;
	//@ ensures \result instanceof String;
	public static String make(int c) { return make(c,null,null); }
	
	/**
	 * Clever method that generates a message to send over the network. Will always return a Protocol supported command
	 * 
	 * @param c an integer representing a command
	 * @param t a String object representing the text
	 * @return a String compliant with the protocol
	 */
	
	public static String make(int c, String t) { return make(c,t,null); }
	
	/**
	 * Clever method that generates a message to send over the network. Will always return a Protocol supported command
	 * 
	 * @param c an integer representing a command
	 * @param t a String object representing the text
	 * @param f a String object representing the flags
	 * @return a String compliant with the protocol
	 */
	
	public static String make(int c, String t, String f){
		switch(c){
		case LOGIN:
			return "LOGIN " + t;
		case VSIGN:
			return "VSIGN " + t;
		case START:
			return "START " + t;
		case GTURN:
			return "GTURN " + f;
		case GMOVE:
			return "GMOVE " + t;
		case BCAST:
			return "BCAST " + t;
		case BOARD:
			if(t != null && !t.equals(""))
				return "BOARD " + t;
			else
				return "BOARD";
		case GPLST:
			if(t != null && !t.equals(""))
				return "GPLST " + t;
			else
				return "GPLST";
		case WARNI:
			return "WARNI " + t;
		case PROTO:
			return "PROTO " + t;
		case SINFO:
			return "SINFO " + t;
		case ALIVE:
			return "ALIVE";
		case PLIST:
			return "PLIST " + t;
		case LJOIN:
			return "LJOIN " + t;
		case LEAVE:
			if(t != null && !t.equals(""))
				return "LEAVE " + t;
			else
				return "LEAVE";
		case HELLO:
			return "HELLO " + f;
		case STATE:
			if(f != null && !f.equals(""))
				return "STATE " + f;
			else
				return "STATE";
		case NGAME:
			if(t != null && !t.equals(""))
				return "NGAME " + f + " " + t;
			else
				return "NGAME " + f;
		case ERROR:
			if(f != null && !f.equals(""))
				return "ERROR " + f + " " + t;
			else
				return "ERROR " + t;
		case SCORE:
			return "SCORE " + t;
		case INVIT:
			if(f != null && !f.equals(""))
				return "INVIT " + f + " " + t;
			else
				return "INVIT " + t;
		case CHATM:
			return "CHATM " + t; 
		default:
			return null;
		}	
	}
	
	/**
	 * The parser that returns a Protocol object
	 * @param message a String typically received on the socket
	 * @return a Protocol instance representing the input
	 * @throws ParseException If the command could not be parsed
	 */
	
	//@ requires message.length() > 5;
	//@ ensures \result != null && \result instanceof Protocol;
	
	public static Protocol parse(String message) throws ParseException{
		String temp;
		try{
			temp = message.substring(0, 5);
		}catch(StringIndexOutOfBoundsException e){
			throw new ParseException("UnsupportedOperationException", 0);
		}
		int command = 0;
		
		if(temp.equals("LOGIN")){
			command = LOGIN;
		} else if(temp.equals("VSIGN")){
			command = VSIGN;
		} else if(temp.equals("HELLO")){
			command = HELLO;
		} else if(temp.equals("STATE")){
			command = STATE;
		} else if(temp.equals("NGAME")){
			command = NGAME;
		} else if(temp.equals("START")){
			command = START;
		} else if(temp.equals("GTURN")){
			command = GTURN;
		} else if(temp.equals("GMOVE")){
			command = GMOVE;
		} else if(temp.equals("BOARD")){
			command = BOARD;
		} else if(temp.equals("GPLST")){
			command = GPLST;
		} else if(temp.equals("BCAST")){
			command = BCAST;
		} else if(temp.equals("ERROR")){
			command = ERROR;
		} else if(temp.equals("WARNI")){
			command = WARNI;
		} else if(temp.equals("PROTO")){
			command = PROTO;
		} else if(temp.equals("SINFO")){
			command = SINFO;
		} else if(temp.equals("ALIVE")){
			command = ALIVE;
		} else if(temp.equals("SCORE")){
			command = SCORE;
		} else if(temp.equals("PLIST")){
			command = PLIST;
		} else if(temp.equals("LJOIN")){
			command = SINFO;
		} else if(temp.equals("LEAVE")){
			command = LEAVE;
		} else if(temp.equals("INVIT")){
			command = INVIT;
		} else if(temp.equals("CHATM")){
			command = CHATM;
		} else {
			command = 0; // should kill it
		}

		if(message.trim().length() == 5) return new Protocol(command); //message was only command
		else if(command == 0) throw new ParseException("UnsupportedOperationException", 0);
		
		String rest = message.substring(6);
		
		// ALIVE has no arguments...
		switch(command){
		case LOGIN:
		case VSIGN:
		case START:
		case GTURN:
		case GMOVE:
		case BCAST:
		case BOARD:
		case GPLST:
		case WARNI:
		case PROTO:
		case SINFO:
		case PLIST:
		case LJOIN:
		case LEAVE:
		case CHATM:
			return new Protocol(command,rest); // Pure info
		case HELLO:
		case STATE:
			return new Protocol(command, "", rest); // Pure flags
		case NGAME:
		case ERROR:
		case SCORE:
		case INVIT:
			int x = rest.indexOf(" ");
			if(x == -1) return new Protocol(command, "", rest);
			else return new Protocol(command, rest.substring(x + 1), rest.substring(0,x));
		default:
			return null;
		}
	}
}
