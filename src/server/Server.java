package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.List;

import rolit.Connection;
import rolit.Protocol;

public class Server {
	public static final boolean chatsupport = true;
	public static final boolean lobbysupport = false;
	
	public static final boolean VERBOSE = true;
	
	public static final String serverInfo = "SKJB 9.12.104";
	public static final String protocolInfo = "INF-B 1.4.0";

	private static List<PlayerConnection> connections = new LinkedList<PlayerConnection>();
	private static List<Game> games = new LinkedList<Game>();

	private static int PORT = 2014;
	private ServerSocket serverSock;
	
	/**
	 * Class that creates a threadpool. It assigns a new PlayerConnection to every new connection
	 * and continues to listen.
	 * 
	 * @author Sander, Jacco
	 * @see PlayerConnection
	 * @version 0.9.12
	 */
	public Server(){
		if(VERBOSE) System.out.println("Verbose mode on: printing events");
		
		
		boolean goodPort = false;
		
		
		//@ loop_invariant goodPort == true;
		while(!goodPort){
			try{
				System.out.print("Enter port number: ");
				PORT = Integer.parseInt(readString());
				goodPort = true;
			} catch (NumberFormatException e){
				System.out.println("It is vital that you enter an integer");
			}
		}
		
		PlayerConnection pc;
		while(true){
			try {
				serverSock = new ServerSocket(PORT);
				if(VERBOSE) System.out.print("Socket created. ");
				while(true){
					if(VERBOSE) System.out.println("Awaiting new connection");
					pc = new PlayerConnection(
							new Connection(
									serverSock.accept()
							));
					pc.start();
					connections.add(pc);
					if(VERBOSE) System.out.println("New connection accepted, PlayerConnection started");
				}
			} catch (IOException e) {
				System.out.println("Port number taken");
			}
			
			goodPort = false;
			
			//@ loop_invariant goodPort == true;
			while(!goodPort){
				try{
					System.out.print("Enter port number: ");
					PORT = Integer.parseInt(readString());
					goodPort = true;
				} catch (NumberFormatException e){
					System.out.println("It is vital that you enter an integer");
				}
			}
		}
	}
	
	/**
	 * Kills a certain PlayerConnection, by gently removing it from running games etc.
	 * 
	 * @param pc
	 */

	//@ ensures getConnections().contains(pc) == false;
	public static void kill(PlayerConnection pc){
		connections.remove(pc);
		for (Game g : games)
			g.kill(pc);
		
	}
	/**
	 * Tries to start a new game matching the flags in the original command.
	 * 
	 * @param p the Protocol instance describing the original command
	 * @return A game instance the PlayerConnection can join
	 * @throws PlayerIsPlayingException
	 * @throws PlayerNotOnlineException
	 * @see Protocol
	 */
	//@ requires p != null;
	//@ ensures \result != null;
	//@ ensures \result instanceof Game;
	public static Game requestGame(Protocol p) throws PlayerIsPlayingException, PlayerNotOnlineException {
		if(p.getText() == null || p.getText().equals("D")){ // No flags
			for(Game g : games){
				if(g.isAwaiting()) // Game must not be started
					return g;
			}
		} else if(p.getText().equals("H")){
			for(Game g : games){
				if(g.isAwaiting() && g.getMaxPlayerNumber() == 2){ // Game must not be started and have the same flags
					return g;
				}else if(g.getMaxPlayerNumber() == 0){
					g.setMaxPlayerNumber(2);
					return g;
				}
			}
		} else if(p.getText().equals("I")){
			for(Game g : games){
				if(g.isAwaiting() && g.getMaxPlayerNumber() == 3){ // Game must not be started and have the same flags
					return g;
				}else if(g.getMaxPlayerNumber() == 0){
					g.setMaxPlayerNumber(3);
					return g;
				}
			}
		} else if(p.getText().equals("J")){
			for(Game g : games){
				if(g.isAwaiting() && g.getMaxPlayerNumber() == 4){ // Game must not be started and have the same flags
					return g;
				}else if(g.getMaxPlayerNumber() == 0){
					g.setMaxPlayerNumber(4);
					return g;
				}
			}
		} 
		
		Game game;
		
		if(p.getText() != null && !p.getText().equals("")){ 
			if(p.getText().equals("H")) game = new Game(2); 
			else if(p.getText().equals("I")) game = new Game(3); 
			else if(p.getText().equals("J")) game = new Game(4);
			else game = new Game(0);
		}else{
			game = new Game(0);
		}
		
		games.add(game);
		return game;
	}
	
	/**
	 * 
	 * @return the LinkedList containing all PlayerConnection threads
	 */
	
	//@ pure;
	public static List<PlayerConnection> getConnections(){
		return connections;
	}
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		new Server();
	}
	
	
	/**
	 * Reads a new String instance from the console (user input)
	 * @return A string object representing the user input
	 */
	//@ ensures \result != null;
	static public String readString() {
		String antw = null;
		
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			antw = in.readLine();
		} catch (IOException e) {}

		return (antw == null) ? "" : antw;
	}

}
