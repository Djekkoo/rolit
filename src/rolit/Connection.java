/**
 * 
 */
package rolit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * @author sander
 *
 */
public class Connection {

	private Socket sock;
	private BufferedReader in;
	private BufferedWriter out;
	
	protected boolean disconnected = false;
	
	public Connection(Socket sock) {
		this.sock = sock;
		
		try {
			this.in = new BufferedReader(
					new InputStreamReader(sock.getInputStream()));
			this.out = new BufferedWriter(
					new OutputStreamWriter(sock.getOutputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public boolean send(String message){
		try {
			out.write(cleanUpString(message));
			out.flush();
		} catch (IOException e) {
			this.disconnected = true;
			return false;
		}
		return true;
	}
	
	public String receive(){
		String message = new String();
		try {
			message = in.readLine();
		} catch (IOException e) {
			this.disconnected = true;
		}
		
		if(message == null){
			this.disconnected = true;
		}
		
		return message;
	}
	
	public String getIP(){
		return sock.getInetAddress().getHostAddress();
	}
	
	public boolean isAlive(){
		return !disconnected;
	}
	
	public void kill(){
		try {
			sock.close();
		} catch (IOException e) {
		} finally {
			this.disconnected = true;
		}
	}
	
	private String cleanUpString(String message){
		return (message.replaceAll("\n", "")) + System.lineSeparator();
	}

}
