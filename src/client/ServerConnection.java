package client;

import java.net.Socket;
import java.text.ParseException;

import rolit.Callback;
import rolit.CallbackException;
import rolit.Connection;
import rolit.Protocol;

public class ServerConnection extends Connection implements Runnable {

	private Callback callback;
	private Callback connectionLost;
	
	public ServerConnection(Socket sock, Callback callback, Callback connectionLost) {
		super(sock);
		this.callback = callback;
		this.connectionLost = connectionLost;
	}

	public boolean send(String message) {
		boolean success = super.send(message);
		if (this.disconnected) {
			try {
				this.connectionLost.invoke();
			} catch(CallbackException e) {}
		}
		return success;
	}
	
	@Override
	public void run() {

		String message = this.receive();
		
		while (!this.disconnected && message != null) {
			System.out.println(message);
			try {
				this.callback.invoke(Protocol.parse(message));
			} catch (ParseException | CallbackException e) {
				e.printStackTrace();
			}
			message = this.receive();
		}
		
		try {
			this.connectionLost.invoke();
		} catch(CallbackException e) {}
		
		this.kill();
		
	}

}
