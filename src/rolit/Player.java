package rolit;

public class Player {
	private String name;
	private int position;
	private int base_position;
	private boolean ai = false;
	
	public Player(String name){
		this(name, 0, 0);
	}
	
	public Player(String name, int position){
		this(name, position, 0);
	}
	
	public Player(String name, int position, int client_id){
		this.setName(name);
		this.setPosition(position);
		this.base_position = client_id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
	
	public void setBasePosition(int client_id){
		this.base_position = client_id;
	}

	public boolean isClient() {
		return this.base_position == this.position;
	}
	
	public int getBasePosition(){
		return this.base_position;
	}

	public void setAI(boolean ai) {
		this.ai = ai;
		
	}

	public boolean isAI() {
		return this.ai;
	}
	
}
