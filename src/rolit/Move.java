package rolit;

public class Move {

	private int x;
	private int y;
	private Player player;

	public Move(int i, Player player) {
		this.x = i % Board.BOARD_SIZE;
		this.y = (int)Math.floor(i / Board.BOARD_SIZE);
		this.player = player;
	}
	
	public Move(int x, int y, Player player) {
		this.x = x;
		this.y = y;
		this.player = player;
	}
	
	public int getX(){
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getI() {
		return (y * Board.BOARD_SIZE) + x;
	}
	
	public Player getPlayer() {
		return player;
	}
	
}
