package client;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import com.esotericsoftware.tablelayout.swing.Table;

import rolit.Board;
import rolit.Callback;
import rolit.CallbackException;
import rolit.Move;

public class GUI extends JFrame implements Observer  {
	
	private static final long serialVersionUID = 8123701830318706436L;
	public JButton[] button = new JButton[Board.BOARD_SIZE * Board.BOARD_SIZE];
	private Callback set;
	private Callback chatCB;

	private URL empty_button = getClass().getResource("/res/button.png");
	private URL hint_button = getClass().getResource("/res/hint.png");
	private URL possible_button = getClass().getResource("/res/possible.png");
	private URL red_button = getClass().getResource("/res/red.png");
	private URL green_button = getClass().getResource("/res/green.png");
	private URL yellow_button = getClass().getResource("/res/yellow.png");
	private URL blue_button = getClass().getResource("/res/blue.png");
	
	private static final float MARGIN = 25f;
	private static final int PADDING = 3;
	
	private URL[] button_order = new URL[]{red_button, yellow_button, green_button, blue_button};
	private TextArea chat;
	
	public GUI(Callback set, Callback chat) {
		
		super();
		this.set = set;
		this.chatCB = chat;
		initGUI();
		
	}
	
	public void close() {
		this.setEnabled(false);
		this.setVisible(false);
	}
	
	// generate User interface
	private void initGUI() {

		Container c = this.getContentPane();
		//c.setLayout(new FlowLayout());
		this.setLayout(new BorderLayout());
		
		Table table = new Table();
		c.add(table, BorderLayout.CENTER);
		
		// margin top
		table.addCell(" ").height(MARGIN);
		
		// create buttons
		for(int i = 0; i < this.button.length; i++) {
			
			if (i % Board.BOARD_SIZE == 0) {
				table.row();
				table.addCell(" ").width(MARGIN);
			}
			
			ImageIcon image = new ImageIcon(empty_button);
			this.button[i] = new JButton(image);//"-"+(i < 9 ? "0"+(i+1):(i+1))+"-");
			this.button[i].addActionListener(new bController(i, this.set));
			this.button[i].setBorder(BorderFactory.createEmptyBorder());
			this.button[i].setContentAreaFilled(false);
			
			table.addCell((this.button[i])).height(50).width(50);
		}
		
		table.row();
		table.top().left();
		
		this.chat = new TextArea("Welcome", TextArea.SCROLLBARS_VERTICAL_ONLY, getDefaultCloseOperation());
		JTextField message = new JTextField();
		JButton chatSend = new JButton("Send");
		
		chat.setEditable(false);
		
		ActionListener cl = new chatListener(this.chatCB, message);
		
		message.addKeyListener(new kListener(new Callback(cl, "actionPerformed")));
		chatSend.addActionListener(cl);
		
		Table cTable = new Table();
		c.add(cTable, BorderLayout.EAST);
		cTable.top();
		
		cTable.addCell(" ").height(MARGIN);
		cTable.row();
		
		cTable.addCell(chat).width(250f).height(250f).colspan(3).right();
		cTable.addCell(" ").width(MARGIN);
		cTable.row();
		
		cTable.addCell(" ").height(Math.round(MARGIN/2));
		cTable.row();
		
		cTable.addCell(message).width(180-PADDING).fillY();
		cTable.addCell(" ").width(PADDING);
		cTable.addCell(chatSend).width(70);
		cTable.addCell(" ").width(MARGIN);
		cTable.row();
		
		
		this.setMinimumSize(new Dimension(740, 490));
		this.setSize(this.getMinimumSize());
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
	}
	
	public void appendChat(String user, String message) {
		this.chat.append(System.lineSeparator() + user + ": " + message);
	}
	
	private class kListener implements KeyListener {
		
		private Callback cb;
		
		public kListener(Callback cb) {
			super();
			this.cb = cb;
		}
		
		public void keyPressed(KeyEvent arg0)  {}

		public void keyReleased(KeyEvent arg0) {}

		public void keyTyped(KeyEvent arg0) {
			if (arg0.getKeyChar() == '\n') {
			
				try {
					cb.invoke();
				} catch (CallbackException e) {}
				
			}
		}
		
	}
	
	public class chatListener implements ActionListener {

		private Callback chat;
		private JTextField text;
		public chatListener(Callback chat, JTextField text) {
			super();
			this.chat = chat;
			this.text = text;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {

			this.actionPerformed();
			
		}
		
		public void actionPerformed() {
			String message = text.getText().trim();
			
			if (!message.equals("")) {
					
				try {
					chat.invoke(message);
					text.setText("");
					text.requestFocus(true);
				} catch(Exception e) {}
				
			}
				
		}
		
	}
	
	private class bController implements ActionListener  {

		private int position;
		private Callback set;

		public bController(int position, Callback set) {
			
			super();
			this.position = position;
			this.set = set;
			
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			try {
				set.invoke(position);
			} catch (CallbackException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	public void setButton(int i, URL image, boolean enabled) {
		this.button[i].setIcon(new ImageIcon(image));
	}

	public void update(Observable arg0, Object arg1) {
		
			Move move = (Move) arg1;
			
			URL image = empty_button;
			if(move.getPlayer() != null)
				image = this.button_order[move.getPlayer().getPosition()];
			
			this.setButton(move.getI(), image, false);
		
	}

	public void highlight(int i, int[] possible, int[] reset) {
		
		for (int j : reset) {
			this.button[j].setIcon(new ImageIcon(this.empty_button));
		}
		for (int j : possible) {
			this.button[j].setIcon(new ImageIcon(this.possible_button));
		}
		
		if (i >= 0)
			this.button[i].setIcon(new ImageIcon(this.hint_button));
		
	}

}
