/**
 * 
 */
package server;

import java.io.IOException;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.text.ParseException;

import org.apache.commons.codec.binary.Base64;

import rolit.CapturingException;
import rolit.Connection;
import rolit.Move;
import rolit.NotTouchingException;
import rolit.Player;
import rolit.PositionTakenException;
import rolit.Protocol;
import rolit.Protocol.STATES;

/**
 * The thread that handles all the connections with a single player.
 * 
 * @author Sander, Jacco
 */

public class PlayerConnection extends Thread {
	public static final String CLEAN_NAME_REGEX = "[^a-zA-Z0-9-_=+]";
	
	// For the PKI server:
	public static final String SERVER_ADDRESS = "ss-security.student.utwente.nl";
	public static final int SERVER_PORT = 2013;
	
	private static final int RETRY_AMOUNT = 5;
	
	private Player player;
	private Connection connection;
	
	private boolean authenticated = false;
	private Protocol.STATES status = Protocol.STATES.UNKNOWN;
	
	private boolean chatsupport = false;
	private boolean lobbysupport = false;
	
	private Game game;

	/**
	 * Requires a working connection
	 * 
	 * @param connection a Connection instance that can send and receive to and from a socket
	 */
	public PlayerConnection(Connection connection) {
		this.connection = connection;
	}
	
	/**
	 * The thread function. It loops as long as the connection is alive, parsing text and thus receiving commands, 
	 * taking proper actions accordingly and monitor the connection overall.
	 */
	@Override
	public void run(){
		Protocol p;
		try {
			p = Protocol.parse(connection.receive());
			
			if(p.getCommand() == Protocol.LOGIN 
					&& !p.getName().equals("")){
				
				if(Server.VERBOSE) System.out.println("Received LOGIN from " + connection.getIP());
				
				String name = cleanUpName(p.getName());
				
				for(PlayerConnection pc : Server.getConnections()){
					if(pc.getPlayer() != null && 
							pc.getPlayer().getName().equals(name)){
						pc.forceAlive();
						
						if(pc.isAlive()){
							die("User is already logged in");
							return;
						}else{
							if(Server.VERBOSE) System.out.println("Dead player removed");
							Server.kill(pc);
						}
					}
				}
				
				this.player = new Player(name, 0);
				
				if(Server.VERBOSE) System.out.println("Player created: " + player.getName());
				
				authenticate();
				
				handshake();
				
			} else {
				throw new ParseException("Expected LOGIN", 0);
			}
			
			if(Server.VERBOSE) System.out.println("Free I/O state entered");
			status = lobbysupport ? STATES.LOBBY : STATES.WAITING;
			
			//@ loop_invariant connection.isAlive() == false;
			while(connection.isAlive()){
				String message = connection.receive();
				
				if(message == null) {
					// Should crash if client has disconnected
					connection.send(Protocol.make(Protocol.ALIVE));
					message = connection.receive();
				}
				
				p = Protocol.parse(message);
				
				switch(p.getCommand()){
				case Protocol.ALIVE: // Simple keepalive command, no action required
					break;
				
				case Protocol.STATE:
					sendState();
					break;
					
				case Protocol.NGAME:
					if(game == null)
						requestGame(p);
					else
						connection.send(
								Protocol.make(Protocol.ERROR, "AlreadyIngameException"));
					break;
					
				case Protocol.BOARD:
					connection.send(
							Protocol.make(Protocol.BOARD, game.getBoard().toString()));
					break;
					
				case Protocol.GMOVE:
					try {
						game.makeMove(p.getText(), this.player);
					} catch (PositionTakenException e) {
						connection.send(
								Protocol.make(Protocol.ERROR, null, "LocationTakenException"));
					} catch (NotTouchingException e) {
						connection.send(
								Protocol.make(Protocol.ERROR, "The location does not touch", "InvalidLocationException"));
					} catch (CapturingException e) {
						connection.send(
								Protocol.make(Protocol.ERROR, "The location does not capture", "InvalidLocationException"));
					} catch (NotYourTurnException e) {
						connection.send(
								Protocol.make(Protocol.ERROR, "It is not your turn", "InvalidLocationException"));
					}
					break;

				case Protocol.GPLST:
					if(game != null){
						String names = "";
						for(PlayerConnection pc : game.getPlayers()){
							names += pc.getPlayer().getName() + " ";
						}						
						connection.send(
								Protocol.make(Protocol.GPLST, names.trim()));
						
					}
						
					break;
					
				case Protocol.SCORE:
					//TODO: implement get SCORE list
					
				case Protocol.ERROR:
					System.out.println("Client error: " + p.getText());
					break;
					
				case Protocol.WARNI:
					System.out.println("Client warned: " + p.getText());
					break;
					
				case Protocol.LEAVE:
				case Protocol.LJOIN:
				case Protocol.PLIST:
				case Protocol.INVIT:
					if(!lobbysupport){
						connection.send(
								Protocol.make(Protocol.WARNI, "Client does not support lobby"));
					} else if(!Server.lobbysupport){
						connection.send(
								Protocol.make(Protocol.WARNI, "Server does not support lobby"));
					} else {
						System.out.println("ERROR: Expected action");
					}
					//TODO: implement lobby
					break;
				
				case Protocol.PROTO:
					if(p.getText() == null)
						connection.send(
								Protocol.make(Protocol.SINFO, Server.protocolInfo));
					else if(!p.getText().equals(Server.protocolInfo)){
						connection.send(
								Protocol.make(Protocol.WARNI, "Protocols do not match. Games might be incompatible."));
					}
					
				case Protocol.SINFO:
					if(p.getText() == null)
						connection.send(
								Protocol.make(Protocol.SINFO,Server.serverInfo));
					else {} // Is totally irrelevant
					
				case Protocol.GTURN:
					connection.send(
							Protocol.make(Protocol.WARNI, "Client is not allowed to define a turn"));
					break;
					
				case Protocol.START:
					connection.send(
							Protocol.make(Protocol.WARNI, "Client is not allowed to start a game"));
					break;
					
				case Protocol.CHATM: 
					if(!Server.chatsupport){
						connection.send(
								Protocol.make(Protocol.WARNI, "Server does not support chat"));
					} else { 
						if(!chatsupport)
							connection.send(
									Protocol.make(Protocol.WARNI, "Client does not support chat"));
						
						game.sendChat(this.player, p.getText());
					}
					
					break;
					
				case Protocol.BCAST:
					connection.send(
							Protocol.make(Protocol.WARNI, "Client broadcasting not allowed"));
					break;
					
				case Protocol.HELLO:
					connection.send(
							Protocol.make(Protocol.WARNI, "Already done shaking hands"));
					break;
					
				case Protocol.VSIGN:
					connection.send(
							Protocol.make(Protocol.WARNI, "Already authenticated"));
					break;
					
				case Protocol.LOGIN:
					connection.send(
							Protocol.make(Protocol.WARNI, "Already logged in"));
					break;
				}
					
				
			} // Connection loop
			
		} catch (ParseException e) {
			
			connection.send(
					Protocol.make(Protocol.ERROR, e.getMessage()));
			
		} catch (IOException e){}

		System.out.println(player.getName() + " did not respond anymore");
		die();
	}

	/**
	 * Method that should be called when this player has to be informed about a new move.
	 * @param move a Move instance representing the move
	 */
	public void sendMove(Move move){
		connection.send(
				Protocol.make(Protocol.GMOVE, String.format("%s %s %s", (move.getPlayer().getPosition() + 1), move.getX(), move.getY())));
	}
	
	/**
	 * Method that should be called when this player has to be informed about a new chatmessage
	 * @param message the message -including playername- that has to be transponded
	 */
	public void sendChat(String message){
		connection.send(
				Protocol.make(Protocol.CHATM, message));
	}
	
	/**
	 * Method that sends the current status to the client (for synchronizing purposes)
	 */
	public void sendState(){
		connection.send(
				Protocol.make(Protocol.STATE, null, status.toString()));
	}
	
	/**
	 * Method that informs the player of his game starting
	 * @param players a space seperated String representing the playernames
	 */
	public void startGame(String players){
		this.status = STATES.PLAYING;
		
		connection.send(
				Protocol.make(Protocol.START, players));
	}
	
	/**
	 * Method that informs the player of a next turn
	 * @param i the 0-based integer representing the turn (0 - 3)
	 */
	public void nextTurn(int i){
		connection.send(
				Protocol.make(Protocol.GTURN, null, String.valueOf(i + 1)));
	}
	
	/**
	 * Method that informs the player his game has crashed (score = 0) or ended (score > 0)
	 * @param score
	 */
	public void endGame(int score){
		this.status = STATES.STOPPED;
		sendState();
		
		if(!(score == 0))
			connection.send(
					Protocol.make(Protocol.SCORE, String.valueOf(score)));
		
		this.game = null;
			
	}
	
	/**
	 * Forces an alive command to see if the player hasn't crashed
	 */
	public void forceAlive(){
		connection.send(
				Protocol.make(Protocol.ALIVE));
	}
	
	/**
	 * 
	 * @return whether or not the player is authenticated 
	 */
	//@ pure;
	public boolean isAuthenticated(){
		return authenticated;
	}
	
	/**
	 * 
	 * @return a Player object representing the player
	 */
	//@ pure;
	public Player getPlayer(){
		return player;
	}
	
	/**
	 * 
	 * @return a Game instance if the player is playing or waiting for his game to start
	 */
	//@ pure;
	public Game getGame(){
		return game;
	}
	
	/**
	 * Kills the connection
	 * @param str an error message (without the command itself) to send to the client
	 */
	public void die(String str){
		connection.send(
				Protocol.make(Protocol.ERROR, str));
		die();
	}
	
	/**
	 * Kills the connection
	 */
	//@ ensures (\forall int i; 0 <= i && i < Server.getConnections().size(); Server.getConnections().get(i) != this);
	public void die(){
		connection.kill(); // Make your workers forget you
		Server.kill(this); // Make your boss forget you
	}
	
	/*
	 *  Functionality functions
	 */
	
	private void requestGame(Protocol p){
		try {
			this.status = STATES.STARTING;
			sendState();
			
			this.game = Server.requestGame(p);
			this.game.addPlayer(this);
			if(Server.VERBOSE) System.out.println(player.getName() + " has been assigned a game");
		} catch (PlayerIsPlayingException e) {
			connection.send(Protocol.make(Protocol.ERROR, "PlayerIsPlayingException"));
		} catch (PlayerNotOnlineException e) {
			connection.send(Protocol.make(Protocol.ERROR, "PlayerNotOnlineException"));
		}
	}
	

	/*
	 *  Helper functions
	 */
	
	private String cleanUpName(String name){
		return name.replaceAll(CLEAN_NAME_REGEX, "");
	}
	
	private void handshake() throws IOException{
		String flags = "";
		if(Server.chatsupport) flags = "C";
		if(Server.lobbysupport) flags += "L";
		if(flags.equals("")) flags = "D";
		
		connection.send(
				Protocol.make(Protocol.HELLO, null, flags));
		
		Protocol p;
		try {
			p = Protocol.parse(connection.receive());
			
			int i = 0;
			while(i < RETRY_AMOUNT && (p.getCommand() != Protocol.HELLO)){
				
				connection.send(
						Protocol.make(Protocol.ERROR, "Expected HELLO FLAGS", "UnexpectedOperationException"));

				connection.send(
						Protocol.make(Protocol.HELLO, null, flags));
				
				p = Protocol.parse(connection.receive());
				i++;
			}
			
			if(p.getCommand() != Protocol.HELLO){
				connection.send(
						Protocol.make(Protocol.BCAST, "Dont leave me hangin!"));
				if(Server.VERBOSE) System.out.println("Client won't shake hands. Committing suicide.");
				die();
			}
			
			chatsupport = p.supportChat();
			lobbysupport = p.supportLobby();
			
			if(Server.VERBOSE) System.out.println("Hands have been shaken. Time to rock!");
			
		} catch (ParseException e) {
			connection.send(
					Protocol.make(Protocol.ERROR, e.getMessage()));
		}
	}
	
	private void authenticate() throws IOException{
		try {
			Protocol p;
			
			// Make client sign a random message
			String message = randomString();
			
			connection.send(
					Protocol.make(Protocol.VSIGN, message));
			
			p = Protocol.parse(connection.receive());
			
			// Validate reply
			int i = 0;
			while(i < RETRY_AMOUNT && (p.getCommand() != Protocol.VSIGN || p.getText().equals(""))){

				connection.send(
						Protocol.make(Protocol.ERROR, "Expected VSIGN SIGNDTEXT", "UnexpectedOperationException"));

				connection.send(
						Protocol.make(Protocol.VSIGN, message));
				
				p = Protocol.parse(connection.receive());
				i++;
				
			}
			
			if(p.getCommand() != Protocol.VSIGN || p.getText().equals("")){
				connection.send(
						Protocol.make(Protocol.ERROR, "Suit yourself, ass"));
				if(Server.VERBOSE) System.out.println("Client failed verification. Committing suicide.");
				die();
			}
			
			authenticated = verify(retrievePublicKey(), p.getText(), message);
			
			if(Server.VERBOSE) System.out.println("Client succesfully verified");
			
			
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e){
			e.printStackTrace();
		}
	}
	
	private PublicKey retrievePublicKey() throws IOException, NoSuchAlgorithmException, InvalidKeySpecException{
		// Connect to PKI server
		Socket sock = new Socket(SERVER_ADDRESS, SERVER_PORT);
		Connection PKI = new Connection(sock);
		
		// Request
		PKI.send("PUBLICKEY player_" + player.getName());
		String ans = PKI.receive();
		
		// Validate reply
		if(!( ans.length() > 7 && ans.substring(0, 6).equals("PUBKEY") )) throw new IOException();
		
		// Generate public key from reply
		PublicKey pk = generatePublicKey(ans.substring(7));

		if(Server.VERBOSE) System.out.println("Retrieved PublicKey from PKI server");
		
		return pk;
	}
	
	private PublicKey generatePublicKey(String string) throws NoSuchAlgorithmException, InvalidKeySpecException{
		byte[] rawKey = Base64.decodeBase64(string);
	
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(rawKey);
		KeyFactory fact = KeyFactory.getInstance("RSA");
		return fact.generatePublic(keySpec);
	}
	
	private boolean verify(PublicKey pk, String signedBase64, String original){
		try {
			byte[] signature = Base64.decodeBase64(signedBase64);
			Signature sig = Signature.getInstance("SHA1withRSA");
			sig.initVerify(pk);
			sig.update(original.getBytes());
			return sig.verify(signature);
		} catch (NoSuchAlgorithmException | InvalidKeyException | SignatureException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private static String randomString(){
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[40];
		random.nextBytes(bytes);
		return Base64.encodeBase64String(bytes).replace("=", "");
	}

}
