package rolit;

public class NotTouchingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2351465509126796089L;

	public NotTouchingException(int x, int y) {
		super("Position: ["+x+","+y+"] is not touching another ball!");
	}
	
}
